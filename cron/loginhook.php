<?php
/**
 * ownCloud - purge.
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */
namespace OCA\Purge\Cron;

use OCP\IConfig;
use OCP\IUser;
use OCP\IUserSession;

class LoginHook
{
    /** @var IUserSession */
    private $userSession;

    /** @var  IConfig */
    private $config;

    public function __construct($userSession, $config)
    {
        $this->userSession = $userSession;
        $this->config = $config;
    }

    public function register()
    {
        $callback = function (IUser $user) {
            $this->config->setUserValue($user->getUID(), 'purge', 'first_email', false);
        };
        $this->userSession->listen('\OC\User', 'postLogin', $callback);
    }
}
