<?php
/**
 * Created by PhpStorm.
 * User: tcit
 * Date: 24/10/16
 * Time: 09:28
 */

namespace OCA\Purge\Cron;


use OC\BackgroundJob\TimedJob;
use OCA\Purge\AppInfo\Application;
use OCP\BackgroundJob\IJobList;
use OCP\IConfig;
use OCP\IGroupManager;
use OCP\ILogger;
use OCP\IUser;
use OCP\IUserManager;

class PurgeDeactivated extends TimedJob {

	public function run($argument) {

		$app = new Application();

		$container = $app->getContainer();

		/** @var IConfig $config */
		$config = $container->query('Config');
		$appName = $container->query('AppName');

		/* If purging is disabled, stop here */
		if (!$config->getAppValue($appName, 'activate_purge', false)) {
			return;
		}

		/** @var IUserManager $userManager */
		$userManager = $container->query('ServerContainer')->getUserManager();

		/** @var ILogger $logger */
		$logger = $container->query('ServerContainer')->getLogger();

		/** @var IGroupManager $groupManager */
		$groupManager = $container->query('ServerContainer')->getGroupManager();

		$offset_purge = (int)$config->getAppValue($appName, 'offset_purge', 0);

		$users = $userManager->search(null, 20, $offset_purge);

		$nb_users_deleted = 0;

		foreach ($users as $user) {
			/** @var IUser $user */
			if (!$user->isEnabled() && !$groupManager->isAdmin($user->getUID())) {
				$user->delete();

				$logger->info('Deleted deactivated account ' . $user->getUID() . ' .');

				$nb_users_deleted++;
			}
			$offset_purge++;
		}

		if ($offset_purge > (int) $userManager->countUsers()['Database']) {

			/** @var IJobList $jobList */
			$jobList = $container->query('ServerContainer')->getJobList();
			if ($jobList->has('OCA\Purge\Cron\PurgeDeactivated', null)) {
				$jobList->remove('OCA\Purge\Cron\PurgeDeactivated', null);
			}
			$offset_purge = 0;
		}

		$config->setAppValue($appName, 'nb_users_deleted', ( (int)$config->getAppValue($appName, 'nb_users_deleted', 0) + $nb_users_deleted));

		$config->setAppValue($appName, 'offset_purge', $offset_purge);
	}

}