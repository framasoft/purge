<?php
/**
 * ownCloud - purge
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */

namespace OCA\Purge\Cron;


use OC\BackgroundJob\TimedJob;
use OCA\Purge\AppInfo\Application;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\IContainer;
use OCP\IGroupManager;
use OCP\ILogger;
use OCP\IUser;
use OCP\IUserManager;
use OCP\Mail\IMailer;
use OCP\Util;

class SendEmails extends TimedJob {


	/**
	 * @param $argument
	 */
	public function run($argument) {
		$app = new Application();

		$container = $app->getContainer();

		/** @var IConfig $config */
		$config = $container->query('Config');
		$appName = $container->query('AppName');

		/* If purging is disabled, stop here */
		if ($config->getAppValue($appName, 'activate_purge', false) == 'false' || !$config->getAppValue($appName, 'activate_purge', false)) {
			return;
		}

		/** @var IUserManager $userManager */
		$userManager = $container->query('ServerContainer')->getUserManager();

		/** @var ILogger $logger */
		$logger = $container->query('ServerContainer')->getLogger();

		/** @var IGroupManager $groupManager */
		$groupManager = $container->query('ServerContainer')->getGroupManager();

		/* First warning times */
		$time_before_first_warning = $config->getAppValue($appName, 'time_before_first_warning', 30);
		$type_of_time_1 = $config->getAppValue($appName, 'type_of_time_1', 'days');

		/* Disable dev values if we're not in debug mode */
		// $type_of_time_1 = ($type_of_time_1 === 'dev' && $config->getSystemValue('debug', false)) ? 'months' : $type_of_time_1;

		$time1 = SendEmails::getTime($type_of_time_1, $time_before_first_warning);

		/* Second warning times */
		$time_before_deactivation = $config->getAppValue($appName, 'time_before_deactivation', 30);
		$type_of_time_2 = $config->getAppValue($appName, 'type_of_time_2', 'days');

		/* Disable dev values if we're not in debug mode */
		// $type_of_time_2 = ($type_of_time_1 === 'dev' && $config->getSystemValue('debug', false)) ? 'months' : $type_of_time_2;

		$time2 = SendEmails::getTime($type_of_time_2, $time_before_deactivation);

		/* Offset */
		/** @var integer $offset */
		$offset = (int) $config->getAppValue($appName, 'offset', 0);

		$nb_users_to_process = 20;

		/* Reset offset when we're done through all the users */
		if ($offset > (int) $userManager->countUsers()['Database']) {
			$offset = 0;
		}

		/* Get list of users, $nb_users_to_process by $nb_users_to_process */
		$users = $userManager->search(null, $nb_users_to_process, $offset);

		/* Stats for batch */
		$nb_users_warned = 0;
		$nb_users_deactivated = 0;

		foreach ($users as $user) {
			/** @var IUser $user */

			/**
			 * Ignore already disabled users
			 */
			if (!$user->isEnabled()) {
				continue;
			}
			/**
			 * First warning email
			 */
			if (($user->getLastLogin() < time() - $time1) && // if the user's last login was before the required time
				((int) $config->getUserValue($user->getUID(), $appName, 'first_email', 0) === 0) && // and if he doesn't already had an email
				(!$groupManager->isAdmin($user->getUID()))) { // and if he's not an admin

				/* If he's got an email */
				if ($config->getUserValue($user->getUID(), 'settings', 'email', '') !== '') {

					/* We send an email to the user */
					SendEmails::sendMail($container, $user, 'Votre compte sur %s est inutilisé', 'email.warning.deletion', $time_before_first_warning, $type_of_time_1, $time_before_deactivation, $type_of_time_2);
					$logger->info('Send user .' . $user->getUID() . ' the first warning for inactivity.');
					$nb_users_warned++;

					/* We set in the confg that he was send a first email */
					$config->setUserValue($user->getUID(), $appName, 'first_email', true);

					/* We increase the warned users counter */
					$config->setAppValue($appName, 'warned_users', strval(intval($config->getAppValue($appName, 'warned_users', 0)) + 1));

				} else { // the user most probably removed it from the personal settings panel
					$logger->warning('User ' . $user->getUID() . ' has no email !');
				}
				continue;
			}

			/**
			 * Deactivation email
			 */
			if (($user->getLastLogin() < time() - ($time1 + $time2)) && // if the user still hasn't login for the two times
				((int) $config->getUserValue($user->getUID(), $appName, 'first_email', 0) === 1) && // and if he has received the first email or he never logged in
				($user->getLastLogin() != 0) && // and he has logged in at least once
				!$groupManager->isAdmin($user->getUID())) { // and if he's not an admin

				/* We send him the email to warning him that his account has been deleted */
				SendEmails::sendMail($container, $user, 'Votre compte sur %s a été supprimé', 'email.deletion', $time_before_first_warning, $type_of_time_1, $time_before_deactivation, $type_of_time_2);

				$logger->info('Deactived user .' . $user->getUID() . '.');

				if ($user->isEnabled()) {
					$user->setEnabled(false);
				}

				$nb_users_deactivated++;

				/* We increase the deactived accounts number */
				$config->setAppValue($appName, 'nb_deactivated_accounts', (int) $config->getAppValue($appName, 'nb_deactivated_accounts', 0) + 1);
			}
		}

		$config->setAppValue($appName, 'offset', (string) ($offset + $nb_users_to_process));
		$logger->info('Processed ' . $nb_users_to_process . ' users. ' . $nb_users_warned . ' were warned and ' . $nb_users_deactivated . ' users were deactivated');
	}


	private function sendMail(IContainer $container, IUser $user, $subject, $template, $time_before_first_warning, $type_of_time_1, $time_before_deactivation, $type_of_time_2) {
		/** @var IMailer $mailer */
		$mailer = $container->query('ServerContainer')->getMailer();
		$config = $container->query('Config');
		$defaults = $container->query('Defaults');
		$l10n = $container->query('L10N');

		/* Get user email */
		$useremail = $config->getUserValue($user->getUID(), 'settings', 'email', '');

		$template_var = [
			'sitename' => $defaults->getName(),
			'time1' => $time_before_first_warning,
			'type1' => $type_of_time_1,
			'time2' => $time_before_deactivation,
			'type2' => $type_of_time_2,
			'username' => $user->getUID(),
		];

		/* HTML */
		$html_template = new TemplateResponse('purge', $template . '.html', $template_var, 'blank');
		$html_part = $html_template->render();

		/* Text */
		$body_template = new TemplateResponse('purge', $template . '.text', $template_var, 'blank');
		$text_part = $body_template->render();

		$subject = $l10n->t($subject, [$defaults->getName()]);

		$from = Util::getDefaultEmailAddress('noreply');

		$message = $mailer->createMessage();
		$message->setSubject($subject);
		$message->setFrom([$from => $defaults->getName()]);
		$message->setTo([$useremail => 'Recipient']);
		$message->setPlainBody($text_part);
		$message->setHtmlBody($html_part);
		$mailer->send($message);
	}

	/**
	 * Get timestamp from number and time of time
	 *
	 * @param $type_of_time
	 * @param $time_value
	 * @return integer
	 */
	private function getTime($type_of_time = 'months', $time_value = 10) {
		switch ($type_of_time) {
			case 'days':
				$time = $time_value * 24 * 3600;
				break;

			case 'weeks':
				$time = $time_value * 24 * 3600 * 7;
				break;

			case 'months':
			default:
				$time = $time_value * 24 * 3600 * 31;
				break;
			case 'dev':
				$time = $time_value * 60;
		}
		return $time;
	}
}