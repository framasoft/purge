$(document).ready(function() {

	$('#purgeClean input, #purgeClean select').change(function () {
		var value = '';
		if (this.type === 'checkbox') {
			value = this.checked;
		}
		if (this.type === 'number') {
			value = this.value;
		}
		if ($(this).is('select')) {
			value = this.options[this.selectedIndex].value;
		}
		OC.AppConfig.setValue('purge', $(this).attr('name'), value);
		console.log('Saved config entry ' + $(this).attr('name') + ' with value ' + value + '.');
	});

	$('#purge-clean').keypress(function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
		}
	});

	$('#purge_deactivated').click(function (event) {
		event.preventDefault();

		OC.dialogs.confirm('Do you really want to remove all deactivated accounts ?', 'Remove deactivated accounts ?', function (res) {
			if (res) {
				$.post(OC.generateUrl('/apps/purge/purge'))
					.done(function (data) {
						if (data.activated === false) {
							OC.dialogs.alert('The purge app is disabled !', 'App disabled');
							return;
						}
						if (data.new === false) {
							OC.Notification.showTemporary('A cron job is already running to delete the deactivated users');
						} else {
							OC.Notification.showTemporary('A cron job has been added to delete the deactivated users');
						}
					})
					.fail(function () {
						OC.Notification.showTemporary('Error while speaking to the server');
					})
				;
			}
		}, true);

	});
});
