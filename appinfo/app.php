<?php
/**
 * ownCloud - purge.
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */
namespace OCA\Purge\AppInfo;

use OCP\BackgroundJob\IJobList;

$app = new Application();
$c = $app->getContainer();

/* Register admin panel */
\OCP\App::registerAdmin($c->getAppName(), 'admin');

/* Register login hook */
$c->query('LoginHook')->register();

/* Register cron job */
/** @var IJobList $jobList */
$jobList = $c->query('ServerContainer')->getJobList();
if (!$jobList->has('OCA\Purge\Cron\SendEmails', null)) {
    $jobList->add('OCA\Purge\Cron\SendEmails', null);
}
