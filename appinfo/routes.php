<?php
/**
 * ownCloud - purge.
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */

return ['routes' => [
    array('name' => 'settings#purge', 'url' => '/purge', 'verb' => 'POST'), // purge deactivated accounts (disabled for now)
]];
