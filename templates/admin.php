<?php
script('purge', 'settings');
?>

<form id="purgeClean" class="section">
	<h2><?php p($l->t('Purge unused accounts')); ?></h2>
	<p>
		<input type="checkbox" id="activate_purge" class="checkbox" name="activate_purge" <?php if ($_['activate_purge'] === "true") { ?> checked="checked" value="true" <?php } ?>>
		<label for="activate_purge"><?php p($l->t('Activate purge')); ?></label>
	</p>
	<p style="margin-bottom: 5px;"><?php p($l->t('Number of warned accounts : %s', $_['nb_warned_accounts'])); ?><br><em><?php p($l->t('An user may have been warned multiple times')); ?></em></p>
	<p style="margin-bottom: 5px;"><?php p($l->t('Number of deactivated accounts : %s', $_['nb_deactivated_accounts'])); ?><br></p>
	<p style="margin-bottom: 5px;"><?php p($l->t('Number of deleted accounts : %s', $_['nb_users_deleted'])); ?></p>
	<fieldset>
		<legend><h3><?php p($l->t('First warning')); ?></h3></legend>
		<div style="padding-left: 28px;">
			<p>
				<label for="time_before_first_warning"><?php p($l->t('Time before first warning')); ?></label>
				<input type="number" id="time_before_first_warning" name="time_before_first_warning" value="<?php p($_['time_before_first_warning']); ?>">
			</p>
			<p>
				<label for="type_of_time_1"><?php p($l->t('Type of time')); ?></label>
				<select id="type_of_time_1" name="type_of_time_1">
					<option value="days" <?php p($_['type_of_time_1'] === 'days' ? 'selected="selected"' : ''); ?>><?php p($l->t('Days')); ?></option>
					<option value="weeks" <?php p($_['type_of_time_1'] === 'weeks' ? 'selected="selected"' : ''); ?>><?php p($l->t('Weeks')); ?></option>
					<option value="month" <?php p($_['type_of_time_1'] === 'month' ? 'selected="selected"' : ''); ?>><?php p($l->t('Months')); ?></option>
					<?php if ($_['debug']) { ?>
						<option value="dev" <?php p($_['type_of_time_1'] === 'dev' ? 'selected="selected"' : ''); ?>><?php p($l->t('Dev')); ?></option>
					<?php } ?>
				</select>
			</p>
		</div>
	</fieldset>
	<fieldset>
		<legend><h3><?php p($l->t('Deactivation')); ?></h3></legend>
		<div style="padding-left: 28px;">
			<p>
				<label for="time_before_deactivation"><?php p($l->t('Time before deactivation')); ?></label>
				<input type="number" id="time_before_deactivation" name="time_before_deactivation" value="<?php p($_['time_before_deactivation']); ?>">
			</p>
			<p>
				<label for="type_of_time_2"><?php p($l->t('Type of time')); ?></label>
				<select id="type_of_time_2" name="type_of_time_2">
					<option value="days" <?php p($_['type_of_time_2'] === 'days' ? 'selected="selected"' : ''); ?>><?php p($l->t('Days')); ?></option>
					<option value="weeks" <?php p($_['type_of_time_2'] === 'weeks' ? 'selected="selected"' : ''); ?>><?php p($l->t('Weeks')); ?></option>
					<option value="month" <?php p($_['type_of_time_2'] === 'month' ? 'selected="selected"' : ''); ?>><?php p($l->t('Months')); ?></option>
					<?php if ($_['debug']) { ?>
						<option value="dev" <?php p($_['type_of_time_2'] === 'dev' ? 'selected="selected"' : ''); ?>><?php p($l->t('Dev')); ?></option>
					<?php } ?>
				</select>
			</p>
		</div>
	</fieldset>
	<button id="purge_deactivated"><?php p($l->t('Remove deactived accounts')) ?></button>
</form>
